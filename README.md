# Git hooks

Where you can find some git hooks I use for my professional and personal projects

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Clone the project 

``` bash
git clone git@gitlab.com:florian-hervieu/git-hooks.git git-hooks
```

## Running the installation

Go into the created folder

```bash
cd git-hooks
```
Run install

````bash
./install
````

And give the script the path to the project which you want "git-hooked"

## Authors

* **Florian Hervieu** - *Initial work* - [GitLab](https://gitlab.com/Florian_Hervieu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
